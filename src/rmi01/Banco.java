package rmi01;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Banco extends Remote {
    public void depositar (String conta, float valor) throws RemoteException, BancoException;
    public float saldo (String conta) throws RemoteException, BancoException;
    public boolean sacar (String conta, float valor) throws RemoteException, BancoException;
    public boolean abreConta (String conta, Cliente c) throws RemoteException, BancoException;
    public boolean fechaConta (String conta) throws RemoteException, BancoException;
    public Cliente getCliente (String conta) throws RemoteException, BancoException;
}