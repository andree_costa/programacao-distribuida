package rmi01;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class App01 {

	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException, BancoException {
		Banco bancoStub = (Banco) Naming.lookup("rmi://localhost/meuBanco");
		
		Cliente c1 = new Cliente("Andre");
		Cliente c2 = new Cliente("Maria");
		Cliente c3 = new Cliente("Carol");
		
		bancoStub.abreConta("123456", c1);
		bancoStub.depositar("123456", 1000);
		bancoStub.sacar("123456", 250);
		bancoStub.abreConta("123457", c2);
		bancoStub.depositar("123457", 10000);
		bancoStub.sacar("123457", 2000);
		bancoStub.abreConta("123458", c3);
		bancoStub.depositar("123458", 100);

	}
}
