package rmi01;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class BancoRMI extends UnicastRemoteObject implements Banco {
	ArrayList<Conta> contas = new ArrayList<Conta>();

	protected BancoRMI() throws RemoteException {}

	public void depositar(String conta, float valor) throws RemoteException, BancoException {
		boolean deposited = false; // Saber conseguiu depositar
		float novo;
		for (Conta account : contas) {
			if (account.getCodigo().equalsIgnoreCase(conta)) {
				deposited = true;
				novo = account.getValor() + valor;
				account.setValor(novo);
			}
		}
		
		if(deposited == false) throw  new BancoException("Conta nao existe");

	}

	public float saldo(String conta) throws RemoteException, BancoException {
		for (Conta account : contas)
			if (account.getCodigo().equalsIgnoreCase(conta)) {
				return account.getValor();
			}
		throw  new BancoException("Conta nao existe");
	}

	public boolean sacar(String conta, float valor) throws RemoteException, BancoException {
		float novo;
		for (Conta account : contas)
			if (account.getCodigo().equalsIgnoreCase(conta)) {
				novo = account.getValor() - valor;
				account.setValor(novo);
				return true;
			}

		throw  new BancoException("Conta nao existe");
	}

	public boolean abreConta(String conta, Cliente cliente)
			throws RemoteException, BancoException {
		for (Conta account : contas)
			if(account.getCodigo().equalsIgnoreCase(conta)) throw  new BancoException("Conta ja existe");

		Conta c = new Conta(conta, cliente);
		this.contas.add(c);

		return false;
	}

	public boolean fechaConta(String conta) throws RemoteException, BancoException {
		for (Conta account : contas)
			if (account.getCodigo().equalsIgnoreCase(conta)) {
				contas.remove(account);
				return true;
			}

		throw  new BancoException("Conta nao existe");
	}

	public Cliente getCliente(String conta) throws RemoteException, BancoException {
		for (Conta account : contas)
			if (account.getCodigo().equalsIgnoreCase(conta))
				return account.getCliente();

		throw  new BancoException("Conta nao existe");
	}

	public static void main(String[] args) throws RemoteException, MalformedURLException {
		BancoRMI banco = new BancoRMI();
		LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
		Naming.rebind("meuBanco", banco);
		System.out.println("Banco esta funcionando normalmente!");
		System.out.println(banco.contas);
	}

}
