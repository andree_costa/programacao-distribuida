package rmi01;

import java.io.Serializable;

public class Conta implements Serializable{
	private static final long serialVersionUID = 1L;
	
	String codigo;
	Cliente cliente;
	float valor;
	
	public Conta(String codigo, Cliente cliente){
		this.codigo = codigo;
		this.cliente = cliente;
		this.valor = 0;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "Conta [codigo=" + codigo + ", cliente=" + cliente.getNome() + ", valor="
				+ valor + "]";
	}

}
