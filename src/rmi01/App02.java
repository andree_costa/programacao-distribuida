package rmi01;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class App02 {
	
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException, BancoException {
		Banco bancoStub = (Banco) Naming.lookup("rmi://localhost/meuBanco");
		
		Cliente c1 = bancoStub.getCliente("123457");
		Cliente c2 = bancoStub.getCliente("123456");
		Cliente c3 = bancoStub.getCliente("123458");
		
		System.out.println(c1);
		bancoStub.sacar("123457", 1000);
		System.out.println(bancoStub.saldo("123457"));
		System.out.println(c2);
		System.out.println(bancoStub.saldo("123456"));
		System.out.println(c3);
		bancoStub.depositar("123458", 1000);
		System.out.println(bancoStub.saldo("123458"));
		bancoStub.depositar("123", 1000);

	}
}
