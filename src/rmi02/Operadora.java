package rmi02;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Operadora extends Remote {
	public Linha getLinha(String linha) throws RemoteException;
	public Linha createLinha(String linha) throws RemoteException;
	public void insertCreditos(String linha, float valor) throws RemoteException;

}
