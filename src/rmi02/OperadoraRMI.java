package rmi02;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class OperadoraRMI extends UnicastRemoteObject implements Operadora{
	ArrayList<Linha> linhas;
	
	protected OperadoraRMI() throws RemoteException {
		super();
		linhas = new ArrayList<Linha>();
	}

	@Override
	public Linha getLinha(String linha) throws RemoteException {
		for(Linha l: linhas) {
			if(l.numero.equals(linha))
				return l;
		}
		return null;
	}
	
	@Override
	public Linha createLinha(String linha) throws RemoteException {
		for(Linha l: linhas)
			if(l.numero.equals(linha)){
				return null;
			}
		
		Linha nova = new Linha(linha);
		linhas.add(nova);
		System.out.println(linhas.size());
		return nova;
	}

	@Override
	public void insertCreditos(String linha, float valor) throws RemoteException {
		for(Linha l: linhas) {
			if(l.numero.equals(linha)) {
				l.addSaldo(valor);
			}
		}		
	}

	public static void main(String args[]) throws RemoteException, MalformedURLException {
		OperadoraRMI operadora = new OperadoraRMI();
		LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
		Naming.rebind("MinhaOperadora", operadora);
		System.out.println("Operadora esta funcionando normalmente!");
	}
	
}
