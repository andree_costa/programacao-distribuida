package rmi02;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class AppCliente01 {

	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
		Operadora operadoraStub = (Operadora) Naming.lookup("rmi://localhost/MinhaOperadora");
		
		operadoraStub.createLinha("87894613");
		
	}

}
