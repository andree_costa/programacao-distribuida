package rmi02;

import java.io.Serializable;

public class Linha implements Serializable {
	String numero;
	float saldo;
	
	public Linha(String numero) {
		super();
		this.numero = numero;
		this.saldo = 0;
	}

	public String getNumero() {
		return numero;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public float getSaldo() {
		return saldo;
	}
	
	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}
	
	public void addSaldo(float saldo) {
		this.saldo += saldo;
	}

	@Override
	public String toString() {
		return "Linha [numero=" + numero + ", saldo=" + saldo + "]";
	}

}
