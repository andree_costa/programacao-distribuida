package sockets08;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class Cliente {
	
	public static void main(String[] args) throws IOException {
		InetAddress address = InetAddress.getByName("localhost");
		DatagramSocket socket = new DatagramSocket();

		String requisicao = "Quais opcoes disponiveis?";
		DatagramPacket request = packetDeEnvio(requisicao, address,  1234);
		socket.send(request); // Pergunta ao servidor

		DatagramPacket dg1 = packetDeRecebimento();
		socket.receive(dg1);	
		System.out.println(new String(dg1.getData()).trim());
		
		Scanner teclado = new Scanner(System.in);
		String op1 = teclado.nextLine(); // Pega opcao
		
		DatagramPacket dg2 = packetDeEnvio(op1, dg1.getAddress(), dg1.getPort());
		socket.send(dg2); // Informa opcao para o servidor

		DatagramPacket dg3 = packetDeRecebimento();

		// Dependendo da opcao a qtd de pacotes necessarios muda
		if(valida(op1) && !op1.equals("2")) { // nas opcoes 1 e 3, só eh necessario o recebimento de um pacote
			socket.receive(dg3);
			System.out.println(new String(dg3.getData()).trim());
			
		}else if(valida(op1) && op1.equals("2")) { // Na opcao 2, o Cliente recebe pergunta, envia palavra e recebe resposta
			socket.receive(dg3);

			System.out.println(new String(dg3.getData()).trim()); // Imprime a pergunta do Servidor
			String op2 = teclado.nextLine(); // Pega palavra para ser testada pelo Servidor
			
			DatagramPacket dg4 = packetDeEnvio(op2, dg3.getAddress(), dg3.getPort());
			socket.send(dg4); // envia
			
			DatagramPacket dg5 = packetDeRecebimento();
			socket.receive(dg5); // recebe resultado final
			
			System.out.println(new String(dg5.getData()).trim());
		} else {
			socket.receive(dg3);
			System.out.println(new String(dg3.getData()).trim());
			socket.close();
		}
		
		socket.close();
	}
	
	/*	--------------	FUNCOES	--------------	*/
	
	// Montagem dos pacotes
	public static DatagramPacket packetDeEnvio(String msg, InetAddress endereco ,int porta) {
		return new DatagramPacket(msg.getBytes(), msg.getBytes().length, endereco, porta);
	}
	
	public static DatagramPacket packetDeRecebimento() {
		return new DatagramPacket(new byte[1024],1024);
	}
	
	// Valida opcao escolhida pelo cliente
	static public boolean valida(String opcao){
		String[] possiveis = {"1", "2", "3"};

		for(String p: possiveis)
			if(p.equals(opcao)) return true;
		
		return false;
	}
	
}
