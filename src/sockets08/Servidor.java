package sockets08;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Calendar;
import java.util.Random;

public class Servidor {
	public static void main(String[] args) throws IOException {
		DatagramSocket socket = new DatagramSocket(1234);
		String opcoes = "1 - Me apoia!\n"
					  + "2 - É palindromo?\n"
					  + "3 - Que horas sao?\n"
					  + "Me informe o numero da opcao que vc deseja:";

		do {
			DatagramPacket requisicao = packetDeRecebimento();
			socket.receive(requisicao);
			System.out.println(new String(requisicao.getData()).trim()); // Recebe e imprime pergunta do Cliente

			DatagramPacket dg1 = packetDeEnvio(opcoes, requisicao.getAddress(), requisicao.getPort());
			socket.send(dg1); // Envia opcoes para Cliente
			
			DatagramPacket dg2 = packetDeRecebimento();
			socket.receive(dg2); // Recebe do cliente opcao escolhida

			String opcao = new String(dg2.getData()).trim();
					
			String retorno = null;
			DatagramPacket dg3 = null;
									
			switch (opcao) { // Dependendo da opcao do cliente numero de pacotes necessarios muda
				case "1": // So precisa de um pacote para envio do apoioMoral
					retorno = apoioMoral();
					dg3 = packetDeEnvio(retorno, dg2.getAddress(), dg2.getPort());
					socket.send(dg3);
					break;
					
				case "2": // Precisa de 3 pacotes, um para pergunta, outro receber palavra, outro para resultado final
					retorno = "Qual palavra?";
					dg3 = packetDeEnvio(retorno, dg2.getAddress(), dg2.getPort());
					socket.send(dg3); // pergunta qual palavra

					DatagramPacket dg4 = packetDeRecebimento();
					socket.receive(dg4); // recebe palavra 

					String palavra = new String(dg4.getData()).trim();
					retorno = isPalindromo(palavra);
					DatagramPacket dg5 = packetDeEnvio(retorno, dg4.getAddress(), dg4.getPort());
					socket.send(dg5); // envia resultado final para Cliente

					break;
					
				case "3": // So precisa de um pacote para envio das horas
					retorno = horas();
					dg3 = packetDeEnvio(retorno, dg2.getAddress(), dg2.getPort());
					socket.send(dg3);
					break;
					
				default:
					retorno = "Opção invalida!";
					dg3 = packetDeEnvio(retorno, dg2.getAddress(), dg2.getPort());
					socket.send(dg3);
					break;
			}		

		} while (socket.getInetAddress() != null);
		socket.close();
	}
	
	/*	--------------	FUNCOES	--------------	*/
	
	// Montagem dos pacotes
	public static DatagramPacket packetDeEnvio(String msg, InetAddress endereco ,int porta) {
		return new DatagramPacket(msg.getBytes(), msg.getBytes().length, endereco, porta);
	}
	
	public static DatagramPacket packetDeRecebimento() {
		return new DatagramPacket(new byte[1024],1024);
	}
	
	// retornos para o cliente
	public static String apoioMoral() {
		Random r = new Random();
		String[] apoios = {"vai melhorar", "vai passar", "podia ser pior", "relaxe um pouco"};

		int index = r.nextInt(apoios.length);
		
		return apoios[index];
	}
	
	public static String isPalindromo(String palavra) {
		String normal = palavra;
		String invertida = new StringBuffer(normal).reverse().toString();
		if (normal.equals(invertida)) {
			System.out.println(normal +" = "+invertida);
			return "Palindrome"; 
		} else {
			System.out.println(normal +" != "+invertida);
			return "Not Palindrome";
		}
	}
	
	public static String horas(){
		Calendar calendar = Calendar.getInstance();
		String horas= String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		String minutos = String.valueOf(calendar.get(Calendar.MINUTE));
		
		return  horas + ":" + minutos;
	}
	
}
