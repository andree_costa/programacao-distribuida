# sockets08

Escreva uma aplicação distribuída com sockets UDP em que o cliente envia uma mensagem para um servidor, pedindo as opções de operações disponíveis. O servidor envia as opções e o cliente escolhe o que quer fazer.Considere as seguintes opções: 1 - Servidor envia uma mensagem (uma mensagem de auto-ajuda) para o cliente; 2 - Identificar se uma palavra fornecida pelo cliente é um palíndromo; 3 - Informar hora e data do servidor.
